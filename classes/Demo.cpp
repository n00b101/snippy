  //implementation file for class Demo

   // implementation of our  methods

 #include <iostream>
 #include "Demo.h"//include definition of class Demo


 using namespace std;

 // :: is called 
 // binary scope resolution operator

//default constructor
 Demo::Demo()
 {
   //initialize all data members
   var = 0;
   time = 0.0;
 }


//overloaded c'tor (constructor)
 Demo::Demo( int val, double val2)
 {
   //initialize all data members
  var = val;
  time = val2;
 }

 void Demo::setVar(int val )
 {
   var = val;
 }

 int Demo::getVar()
 {
  return var;
 }

 void Demo::setTime(double val )
 {
   time = val;
 }

 double Demo::getTime()
 {
  return time;
 }
